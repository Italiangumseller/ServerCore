#pragma once
#include "../ServerCore/ServerApp.h"

class GameServerApp : public ServerApp
{
public:
	GameServerApp() {}
	~GameServerApp() {}

public:
	//
	void StartApp();

	//
	bool CreateGlobalObjects();

	//
	bool InitializeGlobalObjects();
};

