#include "PCH_GameServer.h"
#include "GameWorkerThread.h"

extern ThreadManager<GameWorkerThread>* GGameWorkerThreadManager = nullptr;

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GameWorkerThread::StartThread()
{
	// TODO: TLS 객체 여기서 초기화. 아직 없으니 바로 RunThread 호출함.
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GameWorkerThread::RunThread()
{
	std::cout << "GameWorkerThread running!" << std::endl;
	
	while (true)
	{
		WorkerThread::DoIocpJob();

		WorkerThread::DoGlobalAsyncJob();

		WorkerThread::DoTimerJob();

		// 처리할 일 없는데 계속 수행되면 클럭 낭비 => 조건 충족 못하면 sleep 시키기.
	}
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
HANDLE GameWorkerThread::CreateThread(const __int64 threadID)
{
	DWORD dwThreadID;

	return reinterpret_cast<HANDLE>(_beginthreadex(NULL, 0, RunWorkerThread, (LPVOID)threadID, CREATE_SUSPENDED, (UINT*)&dwThreadID));
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
unsigned int __stdcall GameWorkerThread::RunWorkerThread(LPVOID lpParam)
{
	UINT threadID = reinterpret_cast<UINT>(lpParam);

	GGameWorkerThreadManager->StartThreads(threadID);

	return 0;
}
