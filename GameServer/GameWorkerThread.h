#pragma once

class GameWorkerThread : public WorkerThread
{
public:
	GameWorkerThread() {}
	virtual ~GameWorkerThread() {}


public:
	//
	void StartThread() override final;

	//
	void RunThread() override final;

	//
	HANDLE CreateThread(const __int64 threadID) override final;

private:
	//
	static unsigned int WINAPI RunWorkerThread(LPVOID lpParam);
};

extern ThreadManager<GameWorkerThread>* GGameWorkerThreadManager;
