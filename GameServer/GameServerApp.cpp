#include "PCH_GameServer.h"
#include "GameServerApp.h"
#include "GameWorkerThread.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
void GameServerApp::StartApp()
{
    do 
    {
        if (false == CreateGlobalObjects())
            break;

        if (false == InitializeGlobalObjects())
            break;

        GGameWorkerThreadManager->ExitThreads();
    } while (false);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool GameServerApp::CreateGlobalObjects()
{
    bool result = false;

    do
    {
        GGameWorkerThreadManager = new ThreadManager<GameWorkerThread>;
        if (nullptr == GGameWorkerThreadManager)
            break;

        result = true;
    } while (false);


    return result;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
bool GameServerApp::InitializeGlobalObjects()
{
    bool result = false;

    // temporary
    int threadCount = 4;
    //

    do
    {
        if (false == GGameWorkerThreadManager->Init(threadCount))
            break;

        result = true;
    } while (false);

    return result;
}
