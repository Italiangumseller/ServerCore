#pragma once

class BaseThread
{
	HANDLE mHandle = nullptr;

public:
	BaseThread() {};
	virtual ~BaseThread() { CloseHandle(mHandle); }


public:
	// Initialize TLS objs
	virtual void StartThread() = 0;

	// thread loop func
	virtual void RunThread() = 0;

	// 
	virtual HANDLE CreateThread(const __int64 threadID) = 0;
public:
	HANDLE GetThreadHandle() const { return mHandle; }
	void SetThreadHandle(HANDLE handle) { mHandle = handle; }
};

