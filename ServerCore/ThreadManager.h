#pragma once
#include "stdafx.h"

class BaseThread;

template<typename T, typename = typename std::enable_if_t<std::is_base_of_v<BaseThread, T>>>
class ThreadManager
{
	using ThreadList = std::vector<T*>;
	ThreadList mThreadList;

public:
	ThreadManager() {}
	~ThreadManager() {}

public:
	//
	bool Init(const int threadCount)
	{
		if (false == CreateThreads<T>(threadCount))
			return false;

		for (const auto& it : mThreadList)
			ResumeThread(it->GetThreadHandle());

		return true;
	}

	//
	template<typename T>
	bool CreateThreads(const int threadCount)
	{
		for (int i = 0; i < threadCount; ++i)
		{
			T* newThread = new T;

			HANDLE handle = newThread->CreateThread(i);
			if (handle == INVALID_HANDLE_VALUE)
				return false;

			newThread->SetThreadHandle(handle);

			mThreadList.emplace_back(newThread);
		}

		return true;
	}

	//
	void StartThreads(const UINT threadID)
	{
		mThreadList[threadID]->RunThread(); // TODO: 추후 StartThread 호출하도록 변경이 필요함.
	}

	//
	void ExitThreads()
	{
		for (auto& it : mThreadList)
			WaitForSingleObject(it->GetThreadHandle(), INFINITE);
	}
};