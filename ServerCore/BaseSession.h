#pragma once
class BaseSession
{
	// 클라이언트(또는 다른 엔드포인트의 서버)와의 연결 수립 및 통신을 담당하는 클래스.
	SOCKET mSocket;
	
	// 버퍼

public:
	//
	BaseSession() {}
	virtual ~BaseSession() {}

public:
	// 연결 처리하기(GQCS로 ConnectCompletion, AcceptCompletion을 통지받고 처리)
	// 연결 종료처리
	
	// 패킷 수신 비동기 call
	// 패킷 수신 완료 시 처리

	// 패킷 송신 비동기 call
	// 패킷 송신 완료 시 처리

};

