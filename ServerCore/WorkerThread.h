#pragma once
#include "BaseThread.h"

class WorkerThread : public BaseThread
{
public:
	WorkerThread() {}
	virtual ~WorkerThread() {}
	
public:
	//
	void DoIocpJob();

	//
	void DoTimerJob();

	//
	void DoGlobalAsyncJob();

};


