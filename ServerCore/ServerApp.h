#pragma once

// ThreadManager 등 전역 객체 생성, 초기화를 수행하자.
// 서버 올라가기 전에 수행해야 하는 것들.
class ServerApp
{
public:
	ServerApp() {}
	virtual ~ServerApp() {}


public:
	//
	virtual void StartApp() = 0;

	//
	virtual bool CreateGlobalObjects() = 0;

	//
	virtual bool InitializeGlobalObjects() = 0;

};

