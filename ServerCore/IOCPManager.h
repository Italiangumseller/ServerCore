#pragma once
#include "stdafx.h"

class IOCPManager
{
	HANDLE mConnectionIOCompletionPort = 0; // iocp 커널 객체 핸들.
	HANDLE mSendRecvIOCompletionPort = 0;

public:
	IOCPManager() {}
	~IOCPManager() {}

public:
	// Iocp 커널 객체 생성, 초기화
	bool Init()
	{
		bool result = false;

		do
		{
			mConnectionIOCompletionPort = CreateIOCP();
			if (NULL == mConnectionIOCompletionPort)
				break;

			mSendRecvIOCompletionPort = CreateIOCP();
			if (NULL == mSendRecvIOCompletionPort)
				break;

			result = true;
		} while (false);

		return result;
	}

	// 연결된 소켓을 IOCP에 연결.
	bool RegisterSocketToIOCP(HANDLE socket)
	{
		bool result = false;

		do
		{
			// Completion port에 연결했다 다음은?
			if (NULL == CreateIoCompletionPort(socket, mConnectionIOCompletionPort, reinterpret_cast<DWORD>(socket), 0))
			{
				std::cout << "errorCode: " << WSAGetLastError() << std::endl;
				break;
			}
		} while (false);
	
		return result;
	}

private:
	//
	HANDLE CreateIOCP()
	{
		return CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
	}
};